/* Mobilike JS */
(function() {
    var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame ||
    function(callback) {
        window.setTimeout(callback, 1000 / 60);
    };
    window.requestAnimationFrame = requestAnimationFrame;
})();

var score = 0;
function createcizis(){ 
	// find body and cizi sizes
	var bodyWidth = document.body.offsetWidth;
	var bodyHeight = document.body.offsetHeight;
	var ciziwidth = (bodyWidth*20) / 100;
	var ciziheight = ciziwidth-9;
	
	// set random top and left position
	var x = Math.floor(Math.random() * bodyWidth); // left
	var y = Math.floor(Math.random() * bodyHeight); // top
	
	// control to cizi position. If new created cizi overflow the screen, recreate it.
	if(x >= (bodyWidth-ciziwidth)+20 || x < 10 || y < ciziheight){
		createcizis()
	}else{
		var img = document.createElement("img");
		img.src = "images/cizi.png";
		img.className = "cizi";
		img.style.left = x + 'px';
		img.style.top = -y + 'px';
		document.getElementById("biscuits").appendChild(img);
	}
}


function animatecizis(){
	if(! hasClass(document.body, "complete")){
		var bodyWidth = document.body.offsetWidth;
		var bodyHeight = document.body.offsetHeight;
		var biscuits = document.getElementById("biscuits");
		var cizi = biscuits.getElementsByClassName("cizi");
		var cizilength = cizi.length;
		
		// get plate
		var plate = [];
		plate["width"] = parseInt(getplate()["pwidth"],10);
		plate["height"] = parseInt(getplate()["pheight"],10);
		plate["left"] = parseInt(getplate()["pleft"],10);
		plate["top"] = parseInt(getplate()["ptop"],10);
		
		// get ciziwidth
		var ciziwidth = (bodyWidth*20) / 100;
		
		// check cizi length.
		if(cizilength < 5){
			createcizis();
		}
		
		
		var cizis = [];
		for(var i = 0; i < cizilength; i++) {
			var topVal = parseInt(cizi[i].style.top, 10);
			var leftVal = parseInt(cizi[i].style.left, 10);
	
			// if overflow an cizi, remove it and create one
			if(topVal > bodyHeight){
				createcizis();
				cizi[i].remove();
			}else if(topVal >= parseInt(plate["top"]) && (leftVal >= plate["left"] && leftVal <= ((plate["left"]+plate["width"]))-ciziwidth ) ){
				cizi[i].className = "cizi2";
				score = score+1;
				document.getElementById("score").innerHTML = score;
				createcizis();
			}else{
				cizi[i].style.top = (topVal + 10) + "px";	
			}
		}
		var positionTimer = setTimeout(animatecizis, 25);
	}else{
		alert("bitti..");
	}
}

function getplate(){
	var plate = document.getElementById("plate");
	var platewidth = plate.offsetWidth;
	var plateheight = plate.offsetHeight;
	var plateleft = plate.offsetLeft;
	var platetop = plate.offsetTop;
	
	return {
		"pwidth":platewidth,
		"pheight":plateheight,
		"pleft":plateleft,
		"ptop":platetop
	}
}

function init(){
	document.body.className = "begin";
	timer(66000,
    function(timeleft) {
        document.getElementById('count').innerHTML = timeleft;
        if(timeleft == 60){
	        createcizis();
	        animatecizis();
        }
        
    },
    function() {
        document.body.className = "begin complete";
    });
    
    if (window.DeviceOrientationEvent) {
	   window.addEventListener("deviceorientation", deviceOrientationListener);
	}
    
}

function deviceOrientationListener(event) {
	//document.getElementById("beta").innerHTML = Math.round(event.gamma);
	var plate = document.getElementById("plate");
	plate.style.left = Math.round(event.gamma);
	
	/*
	0 => 25%
	1 => 26%
	2 => 27%
	
	-1 = 24%*/
}



function timer(time,update,complete) {
    var start = new Date().getTime();
    var interval = setInterval(function() {
        var now = time-(new Date().getTime()-start);
        if( now <= 0) {
            clearInterval(interval);
            complete();
        }
        else update(Math.floor(now/1000));
    },1000); // the smaller this number, the more accurate the timer will be
}


function hasClass(ele,cls) {
     return ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
}

window.onload = function(){
	init();
}